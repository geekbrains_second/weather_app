package online.beslim.weatherapp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import online.beslim.weatherapp.Database.CityDBAssist;
import online.beslim.weatherapp.Database.DbScheme.CityTable;
import online.beslim.weatherapp.Database.WeatherCursorWrapper;
import online.beslim.weatherapp.WeatherModel.CityWeather;

public class CityLab {
    private static CityLab cityLab;
    private App mContext;
    private SQLiteDatabase cityDb;

    private CityLab() {
        mContext = App.getInstance();
        cityDb = new CityDBAssist(mContext).getWritableDatabase();
    }

    public static CityLab get() {
        if (cityLab == null) {
            cityLab = new CityLab();
        }
        return cityLab;
    }

    private static ContentValues getContentValues(CityWeather cityWeather) {
        ContentValues values = new ContentValues();
        values.put(CityTable.Cols.CITY, cityWeather.getName());
        values.put(CityTable.Cols.TEMP, cityWeather.getMain().getTemp());
        values.put(CityTable.Cols.HUMIDITY, cityWeather.getMain().getHumidity());
        values.put(CityTable.Cols.PRESSURE, cityWeather.getMain().getPressure());
        values.put(CityTable.Cols.SUNRISE, cityWeather.getSys().getSunrise());
        values.put(CityTable.Cols.SUNSET, cityWeather.getSys().getSunset());
        values.put(CityTable.Cols.WIND, cityWeather.getWind().getSpeed());
        values.put(CityTable.Cols.LAT, cityWeather.getCoord().getLat());
        values.put(CityTable.Cols.LON, cityWeather.getCoord().getLon());
        return values;
    }

    public void addWeather(CityWeather cityWeather) {
        ContentValues values = getContentValues(cityWeather);
        cityDb.insert(CityTable.NAME, null, values);
    }

    public void updateWeather(CityWeather cityWeather) {
        ContentValues values = getContentValues(cityWeather);
        cityDb.update(CityTable.NAME,
                values,
                CityTable.Cols.CITY + " = ?",
                new String[]{cityWeather.getName()}
        );
    }

    public CityWeather getWeather(String cityName) {
        WeatherCursorWrapper cursor = queryWeather(
                CityTable.Cols.CITY + " = ?",
                new String[]{cityName}
        );
        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getWeather();
        } finally {
            cursor.close();
        }

    }

    private WeatherCursorWrapper queryWeather(String whereClause, String[] whereArgs) {
        Cursor cursor = cityDb.query(
                CityTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new WeatherCursorWrapper(cursor);
    }
}
