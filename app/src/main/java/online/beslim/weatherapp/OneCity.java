package online.beslim.weatherapp;

import java.io.Serializable;

public class OneCity implements Serializable {

    private final String name;
    private final String description;

    OneCity(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String name() {
        return name;
    }

    public String description() {
        return description;
    }
}
