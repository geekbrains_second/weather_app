package online.beslim.weatherapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class OneCityFragment extends Fragment {

    public static final String CITY_ID = "city_name";
    private TextView cityNameLabel;
    private ImageView cityImage;
    private OneCity city;

    public OneCityFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_one_city, container, false);
        cityNameLabel = view.findViewById(R.id.fragment_one_city_label);
        cityImage = view.findViewById(R.id.fragment_one_city_image);
        cityNameLabel.setText(city.name());
        cityImage.setImageResource(App.getImage(city.name()));
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Bundle bundle = getArguments();
        if (bundle != null) {
            city = (OneCity) bundle.getSerializable(CITY_ID);
        }
    }

    public void updateCity(OneCity city) {
        this.city = city;
        cityNameLabel.setText(city.name());
        cityImage.setImageResource(App.getImage(city.name()));
    }
}
