package online.beslim.weatherapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import online.beslim.weatherapp.Adapter.ViewPagerAdapter;
import online.beslim.weatherapp.Fragments.CurrentWeatherFragment;
import online.beslim.weatherapp.Fragments.ForecastWeatherFragment;
import online.beslim.weatherapp.api.Api;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.toolbar)
    Toolbar toolBar;
    @BindView(R.id.tabview)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    private final List<OneCity> allCity;
    private AllCityFragment allCityFragment;
    private OneCityFragment oneCityFragment;
    @BindView(R.id.nav_view)
    NavigationView navView;
    private WebFragment webFragment;
    private FusedLocationProviderClient fusedLocation;
    private LocationCallback locationCallBack;
    private LocationRequest locationRequest;
    private CurrentWeatherFragment currentWeatherFragment;
    private ForecastWeatherFragment forecastWeatherFragment;
    public MainActivity() {
        allCity = new ArrayList<>();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopService(new Intent(this, SomeService.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        startService(new Intent(this, SomeService.class));
        initUi();

        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            makeLocationRequest();
                            makeLocationCallBack();
                            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            fusedLocation = LocationServices.getFusedLocationProviderClient(MainActivity.this);
                            fusedLocation.requestLocationUpdates(locationRequest, locationCallBack, Looper.myLooper());
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                    }
                }).check();
    }

    private void makeLocationCallBack() {
        locationCallBack = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Api.mCurrentLocation = locationResult.getLastLocation();
                setupViewPager(viewPager);
                tabLayout.setupWithViewPager(viewPager);
            }
        };
        return;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(currentWeatherFragment, "Now");
        adapter.addFragment(forecastWeatherFragment, "5 days");
        viewPager.setAdapter(adapter);

    }

    private void makeLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(60000);
        locationRequest.setFastestInterval(30000);
        locationRequest.setSmallestDisplacement(10.0f);

    }

    private void initCityList() {
        Resources resources = getResources();
        allCity.add(new OneCity(resources.getString(R.string.title_nsk), resources.getString(R.string.description_nsk)));
        allCity.add(new OneCity(resources.getString(R.string.title_msk), resources.getString(R.string.description_msk)));
        allCity.add(new OneCity(resources.getString(R.string.title_spb), resources.getString(R.string.description_spb)));
    }

    private void initUi() {
        setSupportActionBar(toolBar);
        currentWeatherFragment = new CurrentWeatherFragment();
        forecastWeatherFragment = new ForecastWeatherFragment();
        navView.setNavigationItemSelectedListener(item -> {
            int id = item.getItemId();
            Intent intent = null;
            switch (id) {
                case R.id.nav_feedback:
                    intent = new Intent(getApplicationContext(), FeedbackActivity.class);
                    break;
                default:
                    break;
            }
            if (intent != null) {
                startActivity(intent);
            }
            drawerLayout.closeDrawer(GravityCompat.START);
            return true;
        });

    }

    private void replaceWithWebFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.view_pager, webFragment)
                .commit();
    }

    private void replaceWithAllCityFragment() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AllCityFragment.CITY_LIST, (Serializable) allCity);
        allCityFragment.setArguments(bundle);
        allCityFragment.setListener(this::replaceWithOneCityFragment);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.view_pager, allCityFragment)
                .commit();
    }

    private void replaceWithOneCityFragment(int id) {
        Bundle args = new Bundle();
        args.putSerializable(OneCityFragment.CITY_ID, allCity.get(id));
        oneCityFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.view_pager, oneCityFragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (oneCityFragment.isAdded()) {
            replaceWithAllCityFragment();
            return;
        }
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

}
