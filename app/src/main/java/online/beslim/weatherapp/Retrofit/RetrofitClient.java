package online.beslim.weatherapp.Retrofit;

import online.beslim.weatherapp.api.Api;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static volatile RetrofitClient instance;
    private Retrofit mRetrofit;

    public static RetrofitClient getInstance(){
        RetrofitClient localInstance = instance;
        if (localInstance == null){
            synchronized (RetrofitClient.class){
                localInstance = instance;
                if (localInstance == null){
                    instance = localInstance = new RetrofitClient();
                }
            }
        }
        return localInstance;
    }
    private RetrofitClient() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(Api.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    public WeatherInterface getWeatherInterface() {
        return mRetrofit.create(WeatherInterface.class);
    }
}