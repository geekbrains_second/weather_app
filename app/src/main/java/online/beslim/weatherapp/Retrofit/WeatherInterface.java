package online.beslim.weatherapp.Retrofit;

import io.reactivex.Observable;
import online.beslim.weatherapp.WeatherModel.CityWeather;
import online.beslim.weatherapp.WeatherModel.ForecastCityWeather;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherInterface {
    @GET("weather")
    Observable<CityWeather> getWeatherByCoords(@Query("lat") String lat, @Query("lon") String lon, @Query("appid") String appid, @Query("units") String unit);

    @GET("forecast")
    Observable<ForecastCityWeather> getForecastWeatherByCoords(@Query("lat") String lat, @Query("lon") String lon, @Query("appid") String appid, @Query("units") String unit);

    @GET("weather")
    Observable<CityWeather> getWeatherByCityName(@Query("q") String cityName, @Query("appid") String appid, @Query("units") String unit);

    @GET("forecast")
    Observable<ForecastCityWeather> getForecastWeatherByCityName(@Query("q") String cityName, @Query("appid") String appid, @Query("units") String unit);


}
