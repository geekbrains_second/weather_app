package online.beslim.weatherapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AllCityFragment extends Fragment {

    public final static String CITY_LIST = "city_list";
    private final List<OneCity> allCity;
    private OnFragmentInteractionListener mListener;

    public AllCityFragment() {
        allCity = new ArrayList<>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_city, container, false);
        RecyclerView allCityList = view.findViewById(R.id.fragment_all_city_list);
        allCityList.setLayoutManager(new LinearLayoutManager(getContext()));
        allCityList.setAdapter(new AllCityAdapter(allCity, this::onButtonPressed));

        return view;
    }

    public void onButtonPressed(int cityId) {
        if (mListener != null) {
            mListener.onFragmentInteraction(cityId);
        }
    }

    public void setListener(OnFragmentInteractionListener listener) {
        this.mListener = listener;
    }

    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
        if (args != null) {
            Serializable collection = args.getSerializable(CITY_LIST);
            if (collection instanceof List) {
                if (!((List) collection).isEmpty() && ((List) collection).get(0) instanceof OneCity) {
                    List<OneCity> cityList = (List<OneCity>) collection;
                    allCity.clear();
                    allCity.addAll(cityList);
                }
            }
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_all_city, menu);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(int cityId);
    }
}
