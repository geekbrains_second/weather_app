package online.beslim.weatherapp.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import online.beslim.weatherapp.App;
import online.beslim.weatherapp.GlideApp;
import online.beslim.weatherapp.R;
import online.beslim.weatherapp.WeatherModel.ForecastCityWeather;
import online.beslim.weatherapp.api.Api;

public class ForecastWeatherAdapter extends RecyclerView.Adapter<ForecastWeatherAdapter.MyOwnViewHolder> {
    private ForecastCityWeather forecastCityWeather;

    public ForecastWeatherAdapter(ForecastCityWeather forecastCityWeather) {

        this.forecastCityWeather = forecastCityWeather;
    }

    @NonNull
    @Override
    public MyOwnViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(App.getInstance()).inflate(R.layout.forecast_item_layout, viewGroup, false);
        return new MyOwnViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyOwnViewHolder myOwnViewHolder, int i) {
        GlideApp.with(App.getInstance())
                .load(Api.getStringUrlImageForecast(forecastCityWeather.list.get(i), ".png"))
                .into(myOwnViewHolder.imageView);

        myOwnViewHolder.dateTextView.setText(Api.convertUnixToDate(forecastCityWeather.list.get(i).getDt()));
        myOwnViewHolder.tempTextView.setText(forecastCityWeather.list.get(i).getMain().getTempAsString());
    }

    @Override
    public int getItemCount() {
        return forecastCityWeather.list.size();
    }

    class MyOwnViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.weather_temp)
        TextView tempTextView;
        @BindView(R.id.weather_date)
        TextView dateTextView;
        @BindView(R.id.weather_image)
        ImageView imageView;

        MyOwnViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
