package online.beslim.weatherapp.WeatherModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wind {

    @SerializedName("speed")
    @Expose
    private Double speed;
    @SerializedName("deg")
    @Expose
    private double deg;

    public Double getSpeed() {
        return speed;
    }

    public String getSpeedAsString() {
        return speed + "m/s";
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public double getDeg() {
        return deg;
    }

    public void setDeg(double deg) {
        this.deg = deg;
    }

}