package online.beslim.weatherapp.WeatherModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Snow {

    @SerializedName("3h")
    @Expose
    private Double threeHours;

    public Double get3h() {
        return threeHours;
    }

    public void set3h(Double threeHourds) {
        this.threeHours = threeHours;
    }

}