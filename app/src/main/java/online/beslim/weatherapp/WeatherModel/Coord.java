package online.beslim.weatherapp.WeatherModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Coord {

    @SerializedName("lon")
    @Expose
    private Double lon;
    @SerializedName("lat")
    @Expose
    private Double lat;

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public String getStringLatLon() {
        return "[" + lat + " : " + lon + "]";
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }
}