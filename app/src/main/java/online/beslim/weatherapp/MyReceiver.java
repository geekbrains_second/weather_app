package online.beslim.weatherapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {
    public static final String PDU_TYPE = "pdus";

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        SmsMessage[] messages;
        String stringMessage = "";
        String format = bundle.getString("format");
        Object[] pdus = (Object[]) bundle.get(PDU_TYPE);
        if (pdus != null) {
            messages = new SmsMessage[pdus.length];
            for (int i = 0; i < messages.length; i++) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                } else {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                }
                stringMessage += "From " + messages[i].getDisplayOriginatingAddress();
                stringMessage += " : " + messages[i].getMessageBody() + "\n";

                Toast.makeText(context, stringMessage, Toast.LENGTH_LONG).show();

            }
        }
    }
}
