package online.beslim.weatherapp;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FeedbackActivity extends AppCompatActivity {
    private static final int REQUEST_CONTACT = 1;
    @BindView(R.id.search_contact)
    ImageButton searchContact;
    @BindView(R.id.contact_tel)
    EditText telContact;
    @BindView(R.id.message_text)
    EditText messageText;
    @BindView(R.id.send_text)
    ImageButton sendText;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        String phoneId;
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_CONTACT && data != null) {
            Uri contactUri = data.getData();

            String[] queryFieldsId = new String[]{
                    ContactsContract.CommonDataKinds.Phone._ID
            };

            Cursor c = this.getContentResolver().query(contactUri, queryFieldsId, null, null, null);
            try {
                if (c.getCount() == 0) {
                    return;
                }
                c.moveToFirst();
                phoneId = c.getString(0);

                Cursor ph = this.getContentResolver().query
                        (ContactsContract.CommonDataKinds.Phone.CONTENT_URI
                                , null
                                , ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + phoneId
                                , null
                                , null);
                try {
                    if (ph.getCount() == 0) {
                        return;
                    }
                    ph.moveToFirst();
                    String phone = ph.getString(ph.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    telContact.setText(phone);
                } finally {
                    ph.close();
                }

            } finally {
                c.close();
            }
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.SEND_SMS, Manifest.permission.READ_CONTACTS, Manifest.permission.RECEIVE_SMS)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        InitUI();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                    }
                }).check();

    }

    private void InitUI() {
        final Intent pickContact = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        searchContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(pickContact, REQUEST_CONTACT);
            }
        });
        sendText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (telContact.getText().length() != 0) {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(telContact.getText().toString(), null, messageText.getText().toString(), null, null);
                }
            }
        });

    }
}
