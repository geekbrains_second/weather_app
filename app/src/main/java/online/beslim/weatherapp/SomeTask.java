package online.beslim.weatherapp;

import android.os.AsyncTask;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class SomeTask extends AsyncTask<Object, Object, Object> {
    private static final String TAG = "SomeTask";

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d(TAG, "onPreExecute ");
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        Log.d(TAG, "onPostExecute ");
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
