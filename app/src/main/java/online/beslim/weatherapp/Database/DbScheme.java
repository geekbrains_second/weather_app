package online.beslim.weatherapp.Database;

public class DbScheme {
    public static final class CityTable {
        public static final String NAME = "cities";

        public static final class Cols {
            public static final String CITY = "cityname";
            public static final String TEMP = "temperature";
            public static final String WIND = "wind";
            public static final String PRESSURE = "pressure";
            public static final String HUMIDITY = "humidity";
            public static final String SUNRISE = "sunrise";
            public static final String SUNSET = "sunset";
            public static final String LAT = "lat";
            public static final String LON = "lon";

        }
    }
}
