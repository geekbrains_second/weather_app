package online.beslim.weatherapp.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import online.beslim.weatherapp.Database.DbScheme.CityTable;


public class CityDBAssist extends SQLiteOpenHelper {
    private static final int VERSION = 1;

    public CityDBAssist(Context context) {
        super(context, CityTable.NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder builder = new StringBuilder();
        builder
                .append("CREATE TABLE ")
                .append(CityTable.NAME)
                .append(" (")
                .append(" _ID INTEGER PRIMARY KEY AUTOINCREMENT, ")
                .append(CityTable.Cols.CITY)
                .append(" TEXT,")
                .append(CityTable.Cols.TEMP)
                .append(" TEXT,")
                .append(CityTable.Cols.WIND)
                .append(" TEXT,")
                .append(CityTable.Cols.HUMIDITY)
                .append(" TEXT,")
                .append(CityTable.Cols.PRESSURE)
                .append(" TEXT,")
                .append(CityTable.Cols.SUNRISE)
                .append(" TEXT,")
                .append(CityTable.Cols.SUNSET)
                .append(" TEXT,")
                .append(CityTable.Cols.LAT)
                .append(" TEXT,")
                .append(CityTable.Cols.LON)
                .append(" TEXT)");
        db.execSQL(builder.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
