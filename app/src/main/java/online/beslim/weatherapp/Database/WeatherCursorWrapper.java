package online.beslim.weatherapp.Database;


import android.database.Cursor;
import android.database.CursorWrapper;

import online.beslim.weatherapp.Database.DbScheme.CityTable;
import online.beslim.weatherapp.WeatherModel.CityWeather;
import online.beslim.weatherapp.WeatherModel.Coord;
import online.beslim.weatherapp.WeatherModel.Main;
import online.beslim.weatherapp.WeatherModel.Sys;
import online.beslim.weatherapp.WeatherModel.Wind;

public class WeatherCursorWrapper extends CursorWrapper {
    /**
     * Creates a cursor wrapper.
     *
     * @param cursor The underlying cursor to wrap.
     */
    public WeatherCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public CityWeather getWeather() {
        String cityName = getString(getColumnIndex(CityTable.Cols.CITY));
        Double cityWind = getDouble(getColumnIndex(CityTable.Cols.WIND));
        Double cityTemp = getDouble(getColumnIndex(CityTable.Cols.TEMP));
        Integer cityHumidity = getInt(getColumnIndex(CityTable.Cols.HUMIDITY));
        Float cityPressure = getFloat(getColumnIndex(CityTable.Cols.PRESSURE));
        Integer citySunrise = getInt(getColumnIndex(CityTable.Cols.SUNRISE));
        Integer citySunset = getInt(getColumnIndex(CityTable.Cols.SUNSET));
        Double cityLat = getDouble(getColumnIndex(CityTable.Cols.LAT));
        Double cityLon = getDouble(getColumnIndex(CityTable.Cols.LON));
        CityWeather cityWeather = new CityWeather();
        cityWeather.setName(cityName);
        Main main = new Main();
        Wind wind = new Wind();
        Sys sys = new Sys();
        Coord coord = new Coord();
        main.setTemp(cityTemp);
        main.setHumidity(cityHumidity);
        main.setPressure(cityPressure);
        wind.setSpeed(cityWind);
        sys.setSunset(citySunset);
        sys.setSunrise(citySunrise);
        coord.setLat(cityLat);
        coord.setLon(cityLon);
        cityWeather.setMain(main);
        cityWeather.setWind(wind);
        cityWeather.setSys(sys);
        cityWeather.setCoord(coord);
        return cityWeather;
    }
}
