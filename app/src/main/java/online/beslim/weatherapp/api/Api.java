package online.beslim.weatherapp.api;

import android.location.Location;

import java.text.SimpleDateFormat;
import java.util.Date;

import online.beslim.weatherapp.WeatherModel.CityWeather;
import online.beslim.weatherapp.WeatherModel.WeatherList;

public class Api {
    public static final String APP_ID = "de42a5da8e30e09aaf57de064193390d";
    public static final String URL = "https://api.openweathermap.org/data/2.5/";
    public static final String IMAGE_URL = "https://openweathermap.org/img/w/";
    public static Location mCurrentLocation = null;

    public static String convertUnixToDate(Integer dt) {
        Date date = new Date(dt * 1000L);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMMM HH:mm");
        return simpleDateFormat.format(date);
    }

    public static String convertUnixToHour(Integer sunrise) {
        Date date = new Date(sunrise * 1000L);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        return simpleDateFormat.format(date);
    }

    public static String getStringUrlImage(CityWeather cityWeather, String extension) {
        return IMAGE_URL + cityWeather.getWeather().get(0).getIcon() + extension;
    }

    public static String getLatFromLocation() {
        return String.valueOf(mCurrentLocation.getLatitude());
    }

    public static String getLonFromLocation() {
        return String.valueOf(mCurrentLocation.getLongitude());
    }

    public static String getStringUrlImageForecast(WeatherList weatherList, String extension) {
        return IMAGE_URL + weatherList.getCityWeather().get(0).getIcon() + extension;
    }
}
