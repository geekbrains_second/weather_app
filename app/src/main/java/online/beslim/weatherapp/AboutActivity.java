package online.beslim.weatherapp;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AboutActivity extends AppCompatActivity {
    private TextView mTextViewTemperature;
    private TextView mTextViewPHumidity;
    private TextView mTextViewAvailable;
    private TextView mTextViewAvailable2;
    private SensorManager mSensorManager;
    private Button mButtonGo;
    private SomeTask mSomeTask;
    private SensorEventListener mSensorEventListenerTemperature;
    private SensorEventListener mSensorEventListenerHumidity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mTextViewPHumidity = findViewById(R.id.humidity);
        mTextViewTemperature = findViewById(R.id.temperature);
        mTextViewAvailable = findViewById(R.id.available);
        mTextViewAvailable2 = findViewById(R.id.available2);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> listSensor = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        List<String> listSensorType = new ArrayList<>();
        for (int i = 0; i < listSensor.size(); i++) {
            listSensorType.add(listSensor.get(i).getName());
        }

        initListeners();

        mButtonGo = findViewById(R.id.button_go);
        mButtonGo.setOnClickListener(v -> {
            mSomeTask = new SomeTask();
            mSomeTask.execute();
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(mSensorEventListenerTemperature,
                mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE),
                SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(mSensorEventListenerHumidity,
                mSensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY),
                SensorManager.SENSOR_DELAY_FASTEST);
    }


    @Override
    protected void onStop() {
        mSensorManager.unregisterListener(mSensorEventListenerTemperature);
        mSensorManager.unregisterListener(mSensorEventListenerHumidity);
        super.onStop();
    }

    private void initListeners() {
        mSensorEventListenerHumidity = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                float[] values = event.values;
                mTextViewPHumidity.setText("Humidity: " + values[0]);
                mTextViewAvailable.setText("Got this sensor");
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
        mSensorEventListenerTemperature = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                float[] values = event.values;
                mTextViewTemperature.setText("Temp: " + values[0]);
                mTextViewAvailable2.setText("Got this sensor");
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
    }
}
