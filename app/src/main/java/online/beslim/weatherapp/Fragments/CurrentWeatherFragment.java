package online.beslim.weatherapp.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import online.beslim.weatherapp.App;
import online.beslim.weatherapp.CityLab;
import online.beslim.weatherapp.GlideApp;
import online.beslim.weatherapp.R;
import online.beslim.weatherapp.Retrofit.RetrofitClient;
import online.beslim.weatherapp.Retrofit.WeatherInterface;
import online.beslim.weatherapp.WeatherModel.CityWeather;
import online.beslim.weatherapp.api.Api;

/**
 * A simple {@link Fragment} subclass.
 */
public class CurrentWeatherFragment extends Fragment {
    @BindView(R.id.weather_image)
    ImageView imageView;
    @BindView(R.id.city_name)
    TextView cityTextView;
    @BindView(R.id.weather_temp)
    TextView tempTextView;
    @BindView(R.id.weather_humidity)
    TextView humidityTextView;
    @BindView(R.id.weather_pressure)
    TextView pressureTextView;
    @BindView(R.id.weather_sunrise)
    TextView sunriseTextView;
    @BindView(R.id.weather_sunset)
    TextView sunsetTextView;
    @BindView(R.id.weather_wind)
    TextView windTextView;
    @BindView(R.id.weather_date_time)
    TextView dateTextView;
    @BindView(R.id.weather_geo)
    TextView geoTextView;
    @BindView(R.id.weather_panel)
    LinearLayout weatherPanel;

    CompositeDisposable compositeDisposable;
    WeatherInterface weatherService;
    private Unbinder unbinder;
    private CityWeather mCityWeather;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    public CurrentWeatherFragment() {
        compositeDisposable = new CompositeDisposable();
        RetrofitClient retrofit = RetrofitClient.getInstance();
        weatherService = retrofit.getWeatherInterface();
    }

    @Override
    public void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_current_weather, container, false);
        unbinder = ButterKnife.bind(this, v);
        loadingWeatherInfo();
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void loadingWeatherInfo() {
        compositeDisposable.add(weatherService.getWeatherByCoords(Api.getLatFromLocation(),
                Api.getLonFromLocation(),
                Api.APP_ID,
                "metric")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cityWeather -> {
                    GlideApp.with(App.getInstance())
                            .load(Api.getStringUrlImage(cityWeather, ".png"))
                            .into(imageView);
                    CityLab cityLab = CityLab.get();
                    mCityWeather = cityLab.getWeather(cityWeather.getName());
                    if (mCityWeather != null) {
                        updatePrevInfo(cityWeather);
                        cityLab.updateWeather(cityWeather);
                    } else {
                        cityLab.addWeather(cityWeather);
                    }
                    cityTextView.setText(getString(R.string.current_weather, cityWeather.getName()));
                    windTextView.setText(cityWeather.getWind().getSpeedAsString());
                    tempTextView.setText(cityWeather.getMain().getTempAsString());
                    dateTextView.setText(Api.convertUnixToDate(cityWeather.getDt()));
                    pressureTextView.setText(cityWeather.getMain().getPressureAsString());
                    humidityTextView.setText(cityWeather.getMain().getHumidityAsString());
                    sunriseTextView.setText(Api.convertUnixToHour(cityWeather.getSys().getSunrise()));
                    sunsetTextView.setText(Api.convertUnixToHour(cityWeather.getSys().getSunset()));
                    geoTextView.setText(cityWeather.getCoord().getStringLatLon());

                    weatherPanel.setVisibility(View.VISIBLE);
                }, throwable -> Toast.makeText(getActivity(), "" + throwable.getMessage(), Toast.LENGTH_LONG).show())
        );
    }

    private void updatePrevInfo(CityWeather cityWeather) {
        cityTextView.setText(getString(R.string.current_weather, cityWeather.getName()));
        windTextView.setText(cityWeather.getWind().getSpeedAsString());
        tempTextView.setText(cityWeather.getMain().getTempAsString());
        dateTextView.setText(Api.convertUnixToDate(cityWeather.getDt()));
        pressureTextView.setText(cityWeather.getMain().getPressureAsString());
        humidityTextView.setText(cityWeather.getMain().getHumidityAsString());
        sunriseTextView.setText(Api.convertUnixToHour(cityWeather.getSys().getSunrise()));
        sunsetTextView.setText(Api.convertUnixToHour(cityWeather.getSys().getSunset()));
        geoTextView.setText(cityWeather.getCoord().getStringLatLon());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_city_search, menu);

        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                compositeDisposable.add(weatherService.getWeatherByCityName(s,
                        Api.APP_ID,
                        "metric")
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(cityWeather -> {
                            GlideApp.with(App.getInstance())
                                    .load(Api.getStringUrlImage(cityWeather, ".png"))
                                    .into(imageView);
                            CityLab cityLab = CityLab.get();
                            mCityWeather = cityLab.getWeather(cityWeather.getName());
                            if (mCityWeather != null) {
                                updatePrevInfo(cityWeather);
                                cityLab.updateWeather(cityWeather);
                            } else {
                                cityLab.addWeather(cityWeather);
                            }

                            cityTextView.setText(getString(R.string.current_weather, cityWeather.getName()));
                            windTextView.setText(cityWeather.getWind().getSpeedAsString());
                            tempTextView.setText(cityWeather.getMain().getTempAsString());
                            dateTextView.setText(Api.convertUnixToDate(cityWeather.getDt()));
                            pressureTextView.setText(cityWeather.getMain().getPressureAsString());
                            humidityTextView.setText(cityWeather.getMain().getHumidityAsString());
                            sunriseTextView.setText(Api.convertUnixToHour(cityWeather.getSys().getSunrise()));
                            sunsetTextView.setText(Api.convertUnixToHour(cityWeather.getSys().getSunset()));
                            geoTextView.setText(cityWeather.getCoord().getStringLatLon());

                            weatherPanel.setVisibility(View.VISIBLE);
                        }, throwable -> Toast.makeText(getActivity(), "" + throwable.getMessage(), Toast.LENGTH_LONG).show())
                );
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }
}
