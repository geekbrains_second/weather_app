package online.beslim.weatherapp.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import online.beslim.weatherapp.Adapter.ForecastWeatherAdapter;
import online.beslim.weatherapp.R;
import online.beslim.weatherapp.Retrofit.RetrofitClient;
import online.beslim.weatherapp.Retrofit.WeatherInterface;
import online.beslim.weatherapp.api.Api;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForecastWeatherFragment extends Fragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    CompositeDisposable compositeDisposable;
    @BindView(R.id.city_name)
    TextView cityTextView;
    @BindView(R.id.weather_geo)
    TextView geoTextView;
    @BindView(R.id.recycler_forecast)
    RecyclerView forecastRecycler;
    WeatherInterface weatherService;
    private Unbinder unbinder;

    public ForecastWeatherFragment() {
        compositeDisposable = new CompositeDisposable();
        RetrofitClient retrofit = RetrofitClient.getInstance();
        weatherService = retrofit.getWeatherInterface();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_forecast_weather, container, false);
        unbinder = ButterKnife.bind(this, v);
        forecastRecycler.setHasFixedSize(false);
        forecastRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        loadingForecastWeatherInfo();
        return v;
    }

    private void loadingForecastWeatherInfo() {
        compositeDisposable.add(weatherService.getForecastWeatherByCoords(Api.getLatFromLocation(),
                Api.getLonFromLocation(),
                Api.APP_ID,
                "metric")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(forecastCityWeather -> {
                    cityTextView.setText(getString(R.string.forecast_weather, forecastCityWeather.getCity().getName()));
                    geoTextView.setText(forecastCityWeather.getCity().getCoord().getStringLatLon());
                    ForecastWeatherAdapter adapter = new ForecastWeatherAdapter(forecastCityWeather);
                    forecastRecycler.setAdapter(adapter);
                }, throwable -> Toast.makeText(getActivity(), "" + throwable.getMessage(), Toast.LENGTH_LONG).show())

        );
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_city_search, menu);

        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                compositeDisposable.add(weatherService.getForecastWeatherByCityName(s,
                        Api.APP_ID,
                        "metric")
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(forecastCityWeather -> {
                            cityTextView.setText(getString(R.string.forecast_weather, forecastCityWeather.getCity().getName()));
                            geoTextView.setText(forecastCityWeather.getCity().getCoord().getStringLatLon());
                            ForecastWeatherAdapter adapter = new ForecastWeatherAdapter(forecastCityWeather);
                            forecastRecycler.setAdapter(adapter);
                        }, throwable -> Toast.makeText(getActivity(), "" + throwable.getMessage(), Toast.LENGTH_LONG).show())

                );
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }
}
